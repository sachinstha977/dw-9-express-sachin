// default=> ||
// if the value is truthy, it takes the truthy value
// if the value is falsy, it looks for other values
// if all are falsy, it takes the last value
// falsy values=> 0, "", undefined, null

let firstName = "";
let name = firstName || 0 || null || "sachin";
console.log(name);
