// nullish => ??
// for nullish, the falsy value is only undefined and null

let firstName = undefined;
let name = firstName ?? 0 ?? "" ?? "sachin";
console.log(name);
