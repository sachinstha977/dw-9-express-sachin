// Primitive
// string, number, boolean, undefined, null

// Difference between primitive data types and non primitive data types
// ------------------------Difference 1--------------------------------
// In primitive, new memory space is created every time we declare a variable
// but in non-primitive, after declaring variable, it doesn't directly creates memory space but rather
// checks if the variable is copy of another variable or not, if so, the new memory space is not created
// rather the memory space is shared with the copy variable. IN short, it only creates new memory space for new variables.

// Example (Primitive)
let a = 2; // creates a memory location for a with value 2
let b = 1; // creates a memory location for b with value 1
a = 6; // changes the value of a to 6
console.log(a);
console.log(b);

// example (Non-Primitive)
let ar1 = [1, 2, 3, 4]; //creates a memory location for ar1
let ar2 = ar1; //shares memory location with ar1
ar1.push(5);
console.log(ar1);
console.log(ar2);

// -----------------Difference 2---------------------

// in primitive, === checks whether the two values are equal
// in non-primitive, === checks whether the two memory locations are same or not

// Example (Primitive)
let p = 2;
let q = p;
let r = 4;
console.log(p === q); // true
console.log(p === r); // false

// Example (Non-Primitive)
let arr1 = [1, 2, 3];
let arr2 = [1, 2, 3];
let arr3 = arr1;
console.log(arr1 === arr2); //false
console.log(arr1 === arr3); // true

// Example(NonPrimitive)
let array1 = [1,2,4]
let array2 = array1;
console.log(array1 === array2);  //returns true
// console.log([1,2]===[1,2]); //false as there is no memory location allocated
