let { name: studentName, age: studentAge = 20 } = { name: "Krishna", age: 40 };
console.log(studentAge);

// to keep the default value while using alias, always keep behind the alias
