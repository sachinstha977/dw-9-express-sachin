let fun = (a, b, ...c) => {
    console.log(a);
    console.log(b);
    console.log(c);
};
fun (1,2,3,4,5);

// rest operator => ...
// rest operator takes rest of the values
// if rest of value is in the collection of values, then output will be array, if it is in key value pair, it ill be in object