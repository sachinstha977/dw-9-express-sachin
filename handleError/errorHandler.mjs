try {
  let error = new Error("Password must be at least 8 characters long");
  throw error;
} catch (error) {
  console.log("Error encountered: " + error);
}
