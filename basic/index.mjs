import { is18 } from "./hwTask1.mjs";
import { isGreaterThan18 } from "./hwTask2.mjs";
import { criteria } from "./hwTask3.mjs";
import { isEven } from "./hwTask4.mjs";
import { avg } from "./hwTask5.mjs";
import { category } from "./hwTask6.mjs";
