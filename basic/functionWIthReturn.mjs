// function return
// let show = function(){
//     console.log("Hello");
//     return "Manish";
// }
// let s = show();
// console.log(s);
// The code after return does not execute, so write the return at the-
//      last of the function definition
let sum = function () {
  console.log("Hello");
  return 3;
  console.log("I am function");
};
console.log("I am main program");
let s = sum();
console.log(s);
