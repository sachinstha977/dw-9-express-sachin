// JavaScript:
// JS is a single threaded, synchronous, and blocking programming language
// JS is case sensitive

// Data: Data are of three types:
// 1) Numbers
// 2) Strings
// 3) Booleans

// Operators: +, -, *, /

// + operator is also used for concatenating strings.
// Operations are always performed between 2 values

// 1+2=3
// "1"+"2"="12"
// If there is a war between string and Number, the precedence goes to string)
// 1+"2"="12"

Comparator Operator
// ===, !==, <, >, <=, >=
// console.log(3===1);
// &&(AND ampersent) ||(OR pipe)
// && -> True -> if all true
// || -> True -> if one is true
//console.log(!true);
// console.log(!false);
// Rules to name variable
// Variables name must be descriptive and it must 
be in camelCase.
// Do not redefine or redeclare variable.
// In boolean all empty are falsy
// 0 -> false
// 1,2,3,....  -> false
// In string if there is any character then it is 
true
// "" -> false
// "a" -> true
// " " -> true
// "0" -> true

// --------------------------------------------------

// We use backtick to call variables 
// String literal ` `
// The code after return does not execute, so write
// the return at the last of the function definition
