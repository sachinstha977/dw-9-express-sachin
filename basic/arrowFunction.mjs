// arrow function
// let show = ()=>{console.log("Arrow function")}
// show()
// Make a arrow function that takes two input, and return sum of that value
let sum = (n1, n2) => {
  let _sum = n1 + n2;
  return _sum;
};
let __sum = sum(5, 5);
console.log(__sum);
