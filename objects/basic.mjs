// array is made by the combinations of values
// object is made by the combinations of key value pairs
// in the following code, name => key, "sachin"=> value, and name:"sachin"=>property
// in object, duplicate name doesn't exist, if so, new will take place of old values
// in objet, order doesn't matter

let obj = {
  name: "sachin",
  age: 22,
  isMarried: false,
};

// to get the whole object
console.log(obj);

// to get the specific element of the object

console.log(obj.name);
console.log(obj.age);
console.log(obj.isMarried);

// to change the specific element of the object

obj.age = 25;
obj.isMarried = true;

console.log(obj);

// to delete the specific element of the object

delete obj.isMarried;

console.log(obj);
