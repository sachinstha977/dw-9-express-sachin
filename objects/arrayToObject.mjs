// every array cant be converted into objects
// we can convert only those arrays whose structure looks like [["name", "sachin"], ["age",29]]; i.e array of array
// in array of array, the inner array must have length of 2
let array = [["name", "sachin"], ["isMarried", false], ["age",29]];
let object = Object.fromEntries(array);
console.log(object);

// Object.fromEntries(arrayName) is used to convert object into arrays



