let obj = {
    name: "Sachin",
    age: 23,
    isMarried: false
}
let keyArray = Object.keys(obj); //Object.keys(objName) is used to get array of keys of an object
let valueArray = Object.values(obj); //Object.values(objName) is used to get array of values of an object
let propsArray = Object.entries(obj); //Object.entries(objName) is used to get array of properties of an object
console.log(keyArray);
console.log(valueArray);
console.log(propsArray);



