// order matters in array
// order doesn't matter in object as to get the data we use obj.name which have nothing to do with order

let bestPerson = ["Tom", "Harry", "Bob"];
// to get second element
console.log(bestPerson[1]);

// to delete first element
delete bestPerson[0];
console.log(bestPerson);

// to change third element
bestPerson[2] = "james";

console.log(bestPerson);

// -------------------------------------------

let obj = {
  name: "sachin",
  location: "Kathmandu",
  contactNumber: 9808899923,
};
// to get the object
console.log(obj);
// to change the location to gagalphedi

obj.location = "Gagalphedi";

console.log(obj);

// to delete the contact number

delete obj.contactNumber;

console.log(obj);
