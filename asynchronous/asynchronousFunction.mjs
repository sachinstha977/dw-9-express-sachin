console.log("a");
// setTimeout is asynchronous function
// anything that push task to background is called as asynchronous function
setTimeout(() => {
  console.log("first");
}, 1000);
console.log("b");
// My POV
// JavaScript is still synchronous besides it's result because in the above lines of code,
// at first line no.1 will run and it logs 'a' as output
// then line no.2-4 will run which job is to throw the function to the node
// then line no. 5 will execute
// In conclusion, line no.2-4 job is only to throw the function to the node, so hence it is proved that JavaScript is synchronous

// Sir's POV
// setTimeout pass the function to the background(node) and attached timer to it
// the function at node will pass to the memory queue of node after timer has finished
// the function of memory queue will pass to the JS if all of the code of JS gets executed
// JS run the code

// Call stack
// Call stack is the place where javaScript code actually runs
// Call stack first takes the code, runs the code and puffed it
// Only one operation can execute at a time in call stack
// Another operation(code) cannot run in call stack until the call stack is empty
// After the call stack is empty, it looks that if any js code is there to be executed

// Event loop
// It constantly monitors call stack, if the call stack is empty then it takes the function from memory queue and add it to the call stack

// setInterval ( () => {
//     console.log("This is a delayed output");
// },10000
// )
