// set is used to remove duplicate values from the array

// let arr = [2,3,4,2,6,7,4,5,3,6];
// let set = new Set(arr);
// console.log(set);

let filterData = (input) => {
  let inputString = input.map((value, i) => {
    return JSON.stringify(value);
  });
  let removeDuplicate = [...new Set(inputString)];
  let filteredData = removeDuplicate.map((value, i) => {
    return JSON.parse(value);
  });
  return filteredData;
};
let arr = [1, 2, ["a"], ["a"], 1];
let output = filterData(arr);
console.log(output);
