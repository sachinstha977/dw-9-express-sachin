// set is only used to remove the duplicate values of the primitive data types
// however, it is also used to remove duplicate null values 

let uniqueValues = [
  ...new Set([1, 2, 3, 4, { name: "Sachin" }, { name: "Sachin" }]),
];
console.log(uniqueValues);
