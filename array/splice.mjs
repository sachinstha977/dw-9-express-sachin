// let input= [1,2,3,4,5];
// input.splice(1,3, "Toyota", "Kia", false, 7);
// console.log(input);

// 1 is the starting index
// 3 is the number of elements to delete
// "toyota", "Kia", false, 7 are the values to be added

// let output = input.splice(1, 3, "Toyota", "Kia", false, 7);
// it also returns the deleted items which is stored in output
// let output = input.splice(1); -->if the no of items to remove is missing then it will remove all items
