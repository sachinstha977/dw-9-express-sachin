// flat is used to open the array wrapper inside an array
// syntax => array.flat(1);
// the number inside flat is used to declare how many wrapper to open fpr each element
let ar1 = [
  [1, 2],
  3,
  4,
  [
    [5, 6, 7],
    [8, 9],
  ],
  [[[1, 2], [3]], [5]],
];
let ar2 = ar1.flat(3);
console.log(ar2);
