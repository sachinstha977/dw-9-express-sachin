// // map loop must return a value
// let ar1 = [1, 12, 123, 1234];
// let ar2 = ar1.map((value, index) => {
//   return value * 2 + 1;
// });
// console.log(ar2);

// let input = [1, 3, 4, 5];
// let output = input.map((value, i) => {
//   if (value % 2 == 0) {
//     return value * 0;
//   } else {
//     return value * 100;
//   }
// });
// console.log(output)

let name = ['n', 'i', 'T', 'A', 'n'];
let _name = name.map((value, i) => {
    if(i === 0) {
        return value.toUpperCase(); 
    }
    else{
        return value.toLowerCase();
    }
})
console.log(_name);