// let ar1 = [1, 2, 3, 4, 5, 6, 7];

// let ar2 = ar1.filter((value, i) => {
//   return false;
// });
// console.log(ar2);

// filter is used to filter arrays where the output is also array and must have element from the input array
let ar1 = [1,2,"a", "b", 4];
let ar2 = ar1.filter((value,i) => {
  if(typeof value === "number"){
    return true;
  }
  else{
    return false;
  }
})
console.log(ar2);