

// to find truthy values in the current array.
let ar1 = [1, 2, 0, false, "ram", "hari"];
// let ar2 = ar1.filter((value, i) => {
//   if (value) {
//     return true;
//   } else {
//     return false;
//   }
// });
let ar2 = ar1.filter(Boolean); //boolean filters only truthy values
console.log(ar2);