// All methods returns some value but Push, Pop, Unshift, and Shift change original array
//  while reverse and sort methods changes original arrays and also returns some value

// let name = ['n', 'i', 'h', 'c', 'a', 's'];
// let reverseName = name.reverse();
// console.log(reverseName);
// console.log(name);

let str1 = "abc";
// console.log(str1);
// let ar1 = str1.split("");
// // console.log(ar1);
// let ar2 = ar1.reverse();
// // console.log(ar2);
// let str2 = ar2.join("");
// console.log(str2);
let reverseStr = str1.split("").reverse().join("");
console.log(reverseStr);

let input = "My name is";
let output = input.split(" ").reverse().join(" ");
console.log(output);
