// nep.aasu@gmail.com
// 9860678385@@

// let unsortedArray = [5,1,6,4,2,3];
// let sortedArray = unsortedArray.sort();
// console.log(sortedArray);

let ar1 = ['ab', 'cd', 'ce', 'aaav', 'aaag'];
let ar2 = ar1.sort().reverse();
console.log(ar2);

// Number sorting doesn't work properly in JS Example

let arr = [10,9,8];
let arr1 = arr.sort();
console.log(arr1);

// Here, the sorted array is [10,8,9] which is not correct

