// These methods change the original array

// push--> adds element in the last place
// pop--> deletes element from the last place
// unshift--> adds element in the first place
// shift--> deletes element from the first place

// let ar = ["a", "b", "c"];
// ar.push("d");
// console.log(ar);

// let ar1 = [1, 2, 3];
// ar1.pop();
// console.log(ar1);

let cars = ["BMW", "Hyundai", "Kia"];
// cars.pop();
// console.log(cars);

// cars.unshift("Toyota");
// console.log(cars);

cars.shift();
console.log(cars);
