let ar = ["sachin", 22, false];

// array can store multiple values of different data types

// To get whole array
console.log(ar);
// To get the specific element of the array
console.log(ar[0]);
console.log(ar[1]);
console.log(ar[2]);

// to change the specific array elements
ar[1] = 25;
ar[2] = true;

console.log(ar);

// to delete the specific element of the array
delete ar[1];
console.log(ar);
