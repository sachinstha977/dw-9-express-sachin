let cars = ["Hyundai", "Kia", "Toyota", "Ford", "Renault"];
let sliceCar = cars.slice(0, 3);
console.log(sliceCar);

//  if we don't give end index, then slice method will slice till the end
// 1 is the starting index and 5 is the ending index (end index must be always greater than 1)

let str1 = "nitan";
let ar1 = str1.split("");
console.log(ar1);
let slicedAr1 = ar1.slice(0, 1);
console.log(slicedAr1);
let upperCase = slicedAr1.toUpperCase();
