// optional chaining => ?. (syntax)
// for example, if there is no father object inside the object, then normal code(object.father.age) would throw error
// so, instead we use object?.father?.age which will return undefined instead of throwing error

let object = {
  name: "Sachin",
  age: 23,
  location: "Pharping",
  father: {
    age: 45,
    grandfather: {
      age: 85,
    },
  },
};
console.log(object);
console.log(object?.father?.age);
console.log(object?.father?.grandfather?.age);
