let { name, age=30, gender } = { name: "sachin", gender: "male" }; 
// as age is not defined, it will take the default value ,i.e 30
console.log(name, age, gender);