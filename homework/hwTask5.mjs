/*5.
Make a arrow function that takes 3 input as number and return average
of given number*/
export let avg = (num1, num2, num3) => {
  let _avg = (num1 + num2 + num3) / 3;
  return _avg;
};
