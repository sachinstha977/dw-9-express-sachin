// make a arrow function that takes input as "    niTAn   " and return   "nitan"
let name = "niTAn";
let arrowFunction = () => {
  let replacer = name.replaceAll("TA", "ta");
  console.log(replacer);
};
arrowFunction();
