/*3.
Make a arrow function that take a number and return you can
enter room only if the entered number is less than 18 else you
cannot enter*/
export let criteria = (num) => {
  if (num < 18) {
    return "You can enter room.";
  } else {
    return "You cannot enter room.";
  }
};
