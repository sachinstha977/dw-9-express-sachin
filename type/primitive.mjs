// Primitive (number, string, boolean, undefined)

let number = 23;
let string = "Sachin";
let a = undefined;
let b = null;
console.log(typeof string);

console.log(typeof number);

// Non-Primitive (array, object, null)
// Type of all non-primitive data types is object
let array = [1, 2, 3, 4];
let object = { name: "sachin"};
let d = new Set(array);
let e = new Date();
let f = new Error("Invalid Date");
let c = null;
console.log(typeof array);
console.log(typeof object);
console.log(typeof c);

console.log(typeof d);

console.log(typeof e);

console.log(typeof f);
