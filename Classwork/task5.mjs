let products = [
  { name: "earphone", price: 1000 },
  { name: "battery", price: 2000 },
  { name: "charger", price: 500 },
];
// let productsPrice = products.reduce((prev, curr) => {
//   return prev+ curr.price;
// }, 0);
// console.log(productsPrice);

let productsName = products.map((value, i) => {
  return value.name;
});
console.log(productsName);

let productsPrice = products.map((value, i) => {
  return value.price;
});
console.log(productsPrice);

let productsPriceGreaterThan700 = products.filter((value, i) => {
  if (value.price > 700) {
    return true;
  }
});
let productFilterPrice = productsPriceGreaterThan700.map((value, i) => {
  return value.price;
});
let productFilterName = productsPriceGreaterThan700.map((value, i) => {
  return value.name;
});
console.log(productFilterPrice);
console.log(productFilterName);



