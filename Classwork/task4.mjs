// let str1 = "my name is nitan";
// let ar1 = str1.split(" ");
// console.log(ar1);
// let ar2 = ar1.map((value, i) => {
//   let ar3 = value.split("");
//   let ar4 = ar3.map((value, i) => {
//     if (i === 0) {
//       return value[i].toUpperCase();
//     } else {
//       return value.toLowerCase();
//     }
//   });
//   console.log(ar4);

// });

let firstLetterCapital = (input) => {
  let inputArray = input.split("");
  let outputArray = inputArray.map((value, i) => {
    if (i === 0) {
      return value.toUpperCase();
    } else {
      return value.toLowerCase();
    }
  });
  let output = outputArray.join("");
  return output;
};

// my => My

let eachWordFirstCapital = (input) => {  // input = "my name is"
  let inputArray = input.split(" ");     // inputArray = ["my", "name", "is"]   
  let inputArray1 = inputArray.map((value, i) => {
    return firstLetterCapital(value); // ["My", "Name", "Is"]
  });
  let mainOutput = inputArray1.join(" "); //mainOutput = "My Name Is"
  return mainOutput;
};
let input = "my name is sachin shrestha"
console.log(eachWordFirstCapital(input));