// make a function that takes any number  of inputs and returns sum of all those inputs

let sum = (...input) => {
  let output = input.reduce((pre, cur) => {
    return pre + cur;
  }, 0);
  console.log("The sum is " + output);
};
sum(1, 2, 3, 4, 5, ...[2,3]);   