let input = [1, 2, 3, 4, 5];
let output = input.reduce((pre, cur) => {
  return pre * cur;
}, 1);

console.log(output);
