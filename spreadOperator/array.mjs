let array1 = [1, false, ...["a", "b", "c"]];
console.log(array1);

// spread operator => syntax: ...
// spread operator are wrapper opener

let ar2 = [1, 2, 3];
let ar3 = [4, 5, 6];
let ar4 = [...ar2, ...ar3];
console.log(ar4);
